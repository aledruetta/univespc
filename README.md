# Bem vindo ao grupo Univesp C++

Como todo bom engenheiro devemos saber programação. E o conhecimento em C++ é um diferencial. Aqui estamos todos aprendendo, vamos junto nessa aventura de baixo nível da Univesp.

Inicialmente até encontrarmos algo melhor esse é o nosso material.
[http://excript.com/curso-cpp.html](http://excript.com/curso-cpp.html)

Ou se preferir por vídeo [[Youtube, Playlist](https://www.youtube.com/watch?v=5W9YsbqnX0U&list=PLesCEcYj003QTw6OhCOFb1Fdl8Uiqyrqo "Youtube, Playlist")]


### **Enquete**
Não deixe de participar da enquete para conhecermos melhor o grupo!
[Acessar enquete](https://docs.google.com/forms/d/e/1FAIpQLSfS-bFh7PiI-ZE2iNYCV6-KuF759xbld2hollR4XcvZ87L0YA/viewform?usp=sf_link)

### Semana 1
Então, vamos fazer um teste de roteiro semanal:

**Primeira semana:**

- Ler artigo: http://excript.com/cpp/abertura-curso-cpp.html *(10 minutos de leitura)*
- Assistir aula: https://www.youtube.com/watch?v=5W9YsbqnX0U&list=PLesCEcYj003QTw6OhCOFb1Fdl8Uiqyrqo&index=1 *(10 minutos)*
- Tarefa: Configure seu ambiente de trabalho. Consulte no grupo qualquer dúvida ou dificuldade. Escreva um hello world com o seu nome e publique no Grupo Whatsapp um print do resultado.

Quem não fizer vai ganhando pontos... pra sair 😘
